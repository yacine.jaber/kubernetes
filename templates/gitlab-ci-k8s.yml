# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU Lesser General Public License as published by the Free Software Foundation;
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules: Merge Request pipelines
workflow:
  rules:
    # prevent branch pipeline when an MR is open (prefer MR pipeline)
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - when: always

# test job prototype: implement adaptive pipeline rules
.test-policy:
  rules:
    # on tag: auto & failing
    - if: $CI_COMMIT_TAG
    # on ADAPTIVE_PIPELINE_DISABLED: auto & failing
    - if: '$ADAPTIVE_PIPELINE_DISABLED == "true"'
    # on production or integration branch(es): auto & failing
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
    # early stage (dev branch, no MR): manual & non-failing
    - if: '$CI_MERGE_REQUEST_ID == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual
      allow_failure: true
    # Draft MR: auto & non-failing
    - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft:.*/'
      allow_failure: true
    # else (Ready MR): auto & failing
    - when: on_success

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: "$CI_REGISTRY/to-be-continuous/tools/tracking:master"

  # Docker Image with Kubernetes CLI tool (can be overridden)
  K8S_KUBECTL_IMAGE: "registry.hub.docker.com/bitnami/kubectl:latest"
  K8S_KUBE_SCORE_IMAGE: "registry.hub.docker.com/zegl/kube-score:latest"
  K8S_BASE_APP_NAME: "$CI_PROJECT_NAME"
  K8S_SCRIPTS_DIR: "."
  K8S_REVIEW_ENVIRONMENT_SCHEME: "https"

  # default production ref name (pattern)
  PROD_REF: '/^(master|main)$/'
  # default integration ref name (pattern)
  INTEG_REF: '/^develop$/'

stages:
  - package-test
  - deploy
  - production

.k8s-scripts: &k8s-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
    echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
    echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
    echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function fail() {
    log_error "$*"
    exit 1
  }

  function assert_defined() {
    if [[ -z "$1" ]]
    then
      log_error "$2"
      exit 1
    fi
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # import in system
    if echo "$certs" >> /etc/ssl/certs/ca-certificates.crt
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/certs/ca-certificates.crt\\e[0m"
    fi
    if echo "$certs" >> /etc/ssl/cert.pem
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/cert.pem\\e[0m"
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue;
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue;
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue;
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue;
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue;
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue;
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue;
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  # evaluate and export a secret
  # - $1: secret variable name
  function eval_secret() {
    name=$1
    value=$(eval echo "\$${name}")
    case "$value" in
    @b64@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | base64 -d > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded base64 secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding base64 secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @hex@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | sed 's/\([0-9A-F]\{2\}\)/\\\\x\1/gI' | xargs printf > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded hexadecimal secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding hexadecimal secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @url@*)
      url=$(echo "$value" | cut -c6-)
      if command -v curl > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if curl -s -S -f --connect-timeout 5 -o "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully curl'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      elif command -v wget > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if wget -T 5 -O "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully wget'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      else
        log_warn "Couldn't get secret \\e[33;1m${name}\\e[0m: no http client found"
      fi
      ;;
    esac
  }

  function eval_all_secrets() {
    encoded_vars=$(env | grep -Ev '(^|.*_ENV_)scoped__' | awk -F '=' '/^[a-zA-Z0-9_]*=@(b64|hex|url)@/ {print $1}')
    for var in $encoded_vars
    do
      eval_secret "$var"
    done
  }

  # Converts a string to SCREAMING_SNAKE_CASE
  function to_ssc() {
    echo "$1" | tr '[:lower:]' '[:upper:]' | tr '[:punct:]' '_'
  }

  function awkenvsubst() {
    awk '!/# *nosubst/{while(match($0,"[$%]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH-3);val=ENVIRON[var]; gsub(/["\\]/,"\\\\&",val); gsub("\n","\\n",val);gsub("\r","\\r",val); gsub("[$%]{"var"}",val)}}1'
  }

  function exec_hook() {
    if [[ ! -x "$1" ]] && ! chmod +x "$1"
    then
      log_warn "... could not make \\e[33;1m${1}\\e[0m executable: please do it (chmod +x)"
      # fallback technique
      sh "$1"
    else
      "$1"
    fi
  }
  function login() {
    environment_type=$1
    url=$2
    cacert=$3
    token=$4
    namespace=$5
    config=$6

    if [[ -f "$config" ]]
    then
      # $config is a path to a Kuberconfig file
      export KUBECONFIG="$CI_PROJECT_DIR/.kubeconfig"
      cp -f "$config" "$KUBECONFIG"
      log_info "--- using \\e[32mKUBECONFIG\\e[0m provided by env variables (file)"
    elif [[ -n "$config" ]]
    then
      # $config is a Kuberconfig file content
      export KUBECONFIG="$CI_PROJECT_DIR/.kubeconfig"
      echo "$config" > "$KUBECONFIG"
      log_info "--- using \\e[32mKUBECONFIG\\e[0m provided by env variables (content)"
    elif [[ -n "$KUBECONFIG" ]]
    then
      log_info "--- using \\e[32mKUBECONFIG\\e[0m provided by GitLab"

      if [[ -n "$KUBE_CONTEXT" ]]
      then
        kubectl config use-context "$KUBE_CONTEXT"
        log_info "--- switch to the given \\e[32mKUBE_CONTEXT\\e[0m: ${KUBE_CONTEXT}"
      fi
    else
      log_info "--- using exploded \\e[32mKUBECONFIG\\e[0m parameters (env: \\e[33;1m${environment_type}\\e[0m, url: \\e[33;1m${url}\\e[0m, namespace: \\e[33;1m${namespace}\\e[0m)"

      assert_defined "${url}" "Missing required Kubernetes URL. Provide a kubeconfig file or \$K8S_*_URL"
      assert_defined "${token}" "Missing required Kubernetes Token. Provide a kubeconfig file or \$K8S_*_TOKEN"

      export KUBECONFIG="$CI_PROJECT_DIR/.kubeconfig"
      touch "$KUBECONFIG"

      if [[ "$cacert" ]]
      then
        # Cluster config with CA cert
        mkdir -p "$CI_PROJECT_DIR/.kube/certs/k8s-cluster/"
        echo "$cacert" > "$CI_PROJECT_DIR/.kube/certs/k8s-cluster/k8s-ca.crt"
        kubectl config set-cluster k8s-cluster --certificate-authority="$CI_PROJECT_DIR/.kube/certs/k8s-cluster/k8s-ca.crt" --server="$url"
      else
        # Cluster config w/o CA cert
        kubectl config set-cluster k8s-cluster --server="$url"
      fi

      # Credentials config
      kubectl config set-credentials gitlab --token="$token"

      # Context config
      kubectl config set-context gitlab-k8s-cluster --cluster=k8s-cluster --user=gitlab --namespace="$namespace"
      kubectl config use-context gitlab-k8s-cluster
    fi

    kubectl config set-context --current --namespace="$namespace"

    # finally test connection and dump versions
    kubectl ${TRACE+-v=5} version
  }

  function do_kubectl() {
    action=$1

    if [[ "${K8S_KUSTOMIZE_ENABLED}" == "true" ]]
    then
      kustofile=$(ls -1 "$K8S_SCRIPTS_DIR/${environment_type}/kustomization.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/${environment_type}/kustomization.yaml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/kustomization.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/kustomization.yaml" 2>/dev/null || echo "")
      if [[ -z "$kustofile" ]]
      then
        log_error "--- kustomize enabled but nor ${environment_type}/kustomization.yaml neither kustomization.yaml was found"
        exit 1
      fi
      deploymentdir=$(dirname "$kustofile")

      # apply/delete deployment descriptor
      log_info "--- \\e[32mkubectl apply\\e[0m"
      kubectl ${TRACE+-v=5} "$action" -k "$deploymentdir"
    else
      # find deployment file
      deploymentfile=$(ls -1 "$K8S_SCRIPTS_DIR/deployment-${environment_type}.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/deployment.yml" 2>/dev/null || echo "")
      if [[ -z "$deploymentfile" ]]
      then
        log_error "--- deployment file not found"
        exit 1
      fi

      # replace variables (alternative for envsubst which is not present in image)
      awkenvsubst < "$deploymentfile" > generated-deployment.yml

      log_info "--- \\e[32mkubectl apply\\e[0m"
      kubectl ${TRACE+-v=5} "$action" -f ./generated-deployment.yml
    fi
  }

  function deploy() {
    export environment_type=$1
    export environment_name=$2
    environment_url=$3
    # also export environment_name in SCREAMING_SNAKE_CASE format (may be useful with Kubernetes env variables)
    environment_name_ssc=$(to_ssc "$2")
    export environment_name_ssc

    # for backward compatibility
    export stage=$environment_type
    export env=$environment_type
    export appname=$environment_name
    export appname_ssc=$environment_name_ssc

    # variables expansion in $environment_url
    environment_url=$(echo "$environment_url" | awkenvsubst)
    export environment_url
    # extract hostname from $environment_url
    hostname=$(echo "$environment_url" | awk -F[/:] '{print $4}')
    export hostname

    log_info "--- \\e[32mdeploy\\e[0m"
    log_info "--- \$environment_type: \\e[33;1m${environment_type}\\e[0m"
    log_info "--- \$environment_name: \\e[33;1m${environment_name}\\e[0m"
    log_info "--- \$environment_name_ssc: \\e[33;1m${environment_name_ssc}\\e[0m"
    log_info "--- \$hostname: \\e[33;1m${hostname}\\e[0m"

    # unset any upstream deployment env & artifacts
    rm -f kubernetes.env
    rm -f environment_url.txt

    # maybe execute deploy script
    deployscript=$(ls -1 "$K8S_SCRIPTS_DIR/k8s-deploy-${environment_type}.sh" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/k8s-deploy.sh" 2>/dev/null || echo "")
    if [[ -f "$deployscript" ]]
    then
      log_info "--- deploy script (\\e[33;1m${deployscript}\\e[0m) found: execute"
      exec_hook "$deployscript"
    else
      log_info "--- no deploy script found: run template-based deployment"

      # maybe execute pre push script
      prescript="$K8S_SCRIPTS_DIR/k8s-pre-apply.sh"
      if [[ -f "$prescript" ]]; then
        log_info "--- \\e[32mpre-apply hook\\e[0m (\\e[33;1m${prescript}\\e[0m) found: execute"
        exec_hook "$prescript"
      else
        log_info "--- \\e[32mpre-apply hook\\e[0m (\\e[33;1m${prescript}\\e[0m) not found: skip"
      fi

      do_kubectl apply

      # maybe execute post apply script
      postscript="$K8S_SCRIPTS_DIR/k8s-post-apply.sh"
      if [[ -f "$postscript" ]]; then
        log_info "--- \\e[32mpost-apply hook\\e[0m (${postscript}) found: execute"
        exec_hook "$postscript"
      else
        log_info "--- \\e[32mpost-apply hook\\e[0m (${postscript}) not found: skip"
      fi
    fi

    # persist environment url
    if [[ -f environment_url.txt ]]
    then
      environment_url=$(cat environment_url.txt)
      export environment_url
      log_info "--- dynamic environment url found: (\\e[33;1m$environment_url\\e[0m)"
    else
      echo "$environment_url" > environment_url.txt
    fi
    echo -e "environment_type=$environment_type\\nenvironment_name=$environment_name\\nenvironment_url=$environment_url" > kubernetes.env

    # maybe execute readiness check script
    readycheck="$K8S_SCRIPTS_DIR/k8s-readiness-check.sh"
    if [[ -f "$readycheck" ]]; then
      log_info "--- \\e[32mreadiness-check hook\\e[0m (\\e[33;1m${readycheck}\\e[0m) found: execute"
      exec_hook "$readycheck"
    else
      log_info "--- \\e[32mreadiness-check hook\\e[0m (\\e[33;1m${readycheck}\\e[0m) not found: assume app is ready"
    fi
  }

  function rollback() {
    export environment_type=$1
    export environment_name=$2
    environment_url=$3
    # also export environment_name in SCREAMING_SNAKE_CASE format (may be useful with Kubernetes env variables)
    environment_name_ssc=$(to_ssc "$2")
    export environment_name_ssc

    # for backward compatibility
    export stage=$environment_type
    export env=$environment_type
    export appname=$environment_name
    export appname_ssc=$environment_name_ssc

    # variables expansion in $environment_url
    environment_url=$(echo "$environment_url" | awkenvsubst)
    export environment_url
    # extract hostname from $environment_url
    hostname=$(echo "$environment_url" | awk -F[/:] '{print $4}')
    export hostname

    log_info "--- \\e[32mrollback\\e[0m"
    log_info "--- \$environment_type: \\e[33;1m${environment_type}\\e[0m"
    log_info "--- \$environment_name: \\e[33;1m${environment_name}\\e[0m"
    log_info "--- \$environment_name_ssc: \\e[33;1m${environment_name_ssc}\\e[0m"
    log_info "--- \$hostname: \\e[33;1m${hostname}\\e[0m"

    # maybe execute pre cleanup script
    prescript="$K8S_SCRIPTS_DIR/k8s-pre-rollback.sh"
    if [[ -f "$prescript" ]]; then
      log_info "--- \\e[32mpre-rollback hook\\e[0m (\\e[33;1m${prescript}\\e[0m) found: execute"
      exec_hook "$prescript"
    else
      log_info "--- \\e[32mpre-rollback hook\\e[0m (\\e[33;1m${prescript}\\e[0m) not found: skip"
    fi

    # rollback app
    log_info "--- \\e[32mkubectl rollout undo\\e[0m"

    kubectl ${TRACE+-v=5} rollout undo deployment "$environment_name-$environment_type"

    # maybe execute post cleanup script
    postscript="$K8S_SCRIPTS_DIR/k8s-post-rollback.sh"
    if [[ -f "$postscript" ]]; then
      log_info "--- \\e[32mpost-rollback hook\\e[0m (\\e[33;1m${postscript}\\e[0m) found: execute"
      exec_hook "$postscript"
    else
      log_info "--- \\e[32mpost-rollback hook\\e[0m (\\e[33;1m${postscript}\\e[0m) not found: skip"
    fi
  }

  function cleanup() {
    export environment_type=$1
    export environment_name=$2
    # also export environment_name in SCREAMING_SNAKE_CASE format (may be useful with Kubernetes env variables)
    environment_name_ssc=$(to_ssc "$2")
    export environment_name_ssc

    # for backward compatibility
    export stage=$environment_type
    export env=$environment_type
    export appname=$environment_name
    export appname_ssc=$environment_name_ssc

    log_info "--- \\e[32mcleanup\\e[0m"
    log_info "--- \$environment_type: \\e[33;1m${environment_type}\\e[0m"
    log_info "--- \$environment_name: \\e[33;1m${environment_name}\\e[0m"
    log_info "--- \$environment_name_ssc: \\e[33;1m${environment_name_ssc}\\e[0m"

    # maybe execute cleanup script
    cleanupscript=$(ls -1 "$K8S_SCRIPTS_DIR/k8s-cleanup-${environment_type}.sh" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/k8s-cleanup.sh" 2>/dev/null || echo "")
    if [[ -f "$cleanupscript" ]]
    then
      log_info "--- cleanup script (\\e[33;1m${cleanupscript}\\e[0m) found: execute"
      exec_hook "$cleanupscript"
    else
      log_info "--- no cleanup script found: proceed with template-based delete"

      # maybe execute pre cleanup script
      prescript="$K8S_SCRIPTS_DIR/k8s-pre-cleanup.sh"
      if [[ -f "$prescript" ]]; then
        log_info "--- \\e[32mpre-cleanup hook\\e[0m (\\e[33;1m${prescript}\\e[0m) found: execute"
        exec_hook "$prescript"
      else
        log_info "--- \\e[32mpre-cleanup hook\\e[0m (\\e[33;1m${prescript}\\e[0m) not found: skip"
      fi

      # has to be valuated for envsubst
      export hostname=hostname

      do_kubectl delete

      # maybe execute post cleanup script
      postscript="$K8S_SCRIPTS_DIR/k8s-post-cleanup.sh"
      if [[ -f "$postscript" ]]; then
        log_info "--- \\e[32mpost-cleanup hook\\e[0m (\\e[33;1m${postscript}\\e[0m) found: execute"
        exec_hook "$postscript"
      else
        log_info "--- \\e[32mpost-cleanup hook\\e[0m (\\e[33;1m${postscript}\\e[0m) not found: skip"
      fi
    fi
  }

  function score() {
    export environment_type=$1
    if [[ "$K8S_KUSTOMIZE_ENABLED" == "true" ]]
    then
      kustofile=$(ls -1 "$K8S_SCRIPTS_DIR/${environment_type}/kustomization.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/${environment_type}/kustomization.yaml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/kustomization.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/kustomization.yaml" 2>/dev/null || echo "")
      if [[ -z "$kustofile" ]]
      then
        log_error "--- kustomize enabled but nor ${environment_type}/kustomization.yaml neither kustomization.yaml was found"
        exit 1
      fi
      deploymentdir=$(dirname "$kustofile")

      # shellcheck disable=SC2086
      kustomize build "${deploymentdir}" | /usr/bin/kube-score score $K8S_SCORE_EXTRA_OPTS -
    else
      # find deployment file
      deploymentfile=$(ls -1 "$K8S_SCRIPTS_DIR/deployment-${environment_type}.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/deployment.yml" 2>/dev/null || echo "")
      if [[ -z "$deploymentfile" ]]
      then
        log_error "--- deployment file not found"
        exit 1
      fi

      # replace variables (alternative for envsubst which is not present in image)
      awkenvsubst < "$deploymentfile" > generated-deployment.yml

      # shellcheck disable=SC2086
      /usr/bin/kube-score score $K8S_SCORE_EXTRA_OPTS generated-deployment.yml
    fi
  }

  # export tool functions (might be used in after_script)
  export -f log_info log_warn log_error assert_defined rollback awkenvsubst

  unscope_variables
  eval_all_secrets

  # ENDSCRIPT

.k8s-base:
  image:
    name: $K8S_KUBECTL_IMAGE
    entrypoint: [""]
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "kubernetes", "3.4.3" ]
  before_script:
    - *k8s-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"

# Kube-Score job as parallel matrix
k8s-score:
  extends: .k8s-base
  stage: package-test
  image:
    name: $K8S_KUBE_SCORE_IMAGE
    entrypoint: [""]
  script:
    - score "$ENV_TYPE"
  parallel:
    matrix:
      - ENV_TYPE: review
      - ENV_TYPE: integration
      - ENV_TYPE: staging
      - ENV_TYPE: production
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # exclude when $K8S_SCORE_DISABLED is set
    - if: '$K8S_SCORE_DISABLED == "true"'
      when: never
    # exclude review if $K8S_REVIEW_SPACE unset
    - if: '$ENV_TYPE == "review" && ($K8S_REVIEW_SPACE == null || $K8S_REVIEW_SPACE == "")'
      when: never
    # exclude review on integration or prod branch
    - if: '$ENV_TYPE == "review" && ($CI_COMMIT_REF_NAME =~ $INTEG_REF || $CI_COMMIT_REF_NAME =~ $PROD_REF)'
      when: never
    # exclude integration if $K8S_INTEG_SPACE unset
    - if: '$ENV_TYPE == "integration" && ($K8S_INTEG_SPACE == null || $K8S_INTEG_SPACE == "")'
      when: never
    # exclude integration on prod branch
    - if: '$ENV_TYPE == "integration" && $CI_COMMIT_REF_NAME =~ $PROD_REF'
      when: never
    # exclude staging if $K8S_STAGING_SPACE unset
    - if: '$ENV_TYPE == "staging" && ($K8S_STAGING_SPACE == null || $K8S_STAGING_SPACE == "")'
      when: never
    # exclude production if $K8S_PROD_SPACE unset
    - if: '$ENV_TYPE == "production" && ($K8S_PROD_SPACE == null || $K8S_PROD_SPACE == "")'
      when: never
    - !reference [.test-policy, rules]


# Deploy job prototype
# Can be extended to define a concrete environment
#
# @arg ENV_TYPE       : environment type
# @arg ENV_APP_NAME   : env-specific application name
# @arg ENV_APP_SUFFIX : env-specific application suffix
# @arg ENV_API_URL        : env-specific Kubernetes API url
# @arg ENV_SPACE      : env-specific Kubernetes namespace
# @arg ENV_TOKEN      : env-specific Kubernetes API token
# @arg ENV_CA_CERT    : env-specific Kubernetes CA certificate
# @arg ENV_KUBE_CONFIG: env-specific Kubeconfig
.k8s-deploy:
  extends: .k8s-base
  stage: deploy
  variables:
    ENV_APP_SUFFIX: "-$CI_ENVIRONMENT_SLUG"
  before_script:
    - *k8s-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - assert_defined "${ENV_SPACE:-$K8S_SPACE}" 'Missing required env $ENV_SPACE or $K8S_SPACE'
    - login "$ENV_TYPE" "${ENV_API_URL:-$K8S_URL}" "${ENV_CA_CERT:-$K8S_CA_CERT}" "${ENV_TOKEN:-$K8S_TOKEN}" "${ENV_SPACE:-$K8S_SPACE}" "${ENV_KUBE_CONFIG:-${K8S_DEFAULT_KUBE_CONFIG}}"
  script:
    - deploy "$ENV_TYPE" "${ENV_APP_NAME:-${K8S_BASE_APP_NAME}${ENV_APP_SUFFIX}}" "${ENV_URL:-${K8S_ENVIRONMENT_URL:-$ENV_URL_LEGACY}}"
  artifacts:
    name: "$ENV_TYPE env url for $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    when: always
    paths:
      - generated-deployment.yml
      - environment_url.txt
    reports:
      dotenv: kubernetes.env
  environment:
    url: "$environment_url" # can be either static or dynamic

# Cleanup job prototype
# Can be extended for each deletable environment
#
# @arg ENV_TYPE       : environment type
# @arg ENV_APP_NAME   : env-specific application name
# @arg ENV_APP_SUFFIX : env-specific application suffix
# @arg ENV_API_URL        : env-specific Kubernetes API url
# @arg ENV_SPACE      : env-specific Kubernetes namespace
# @arg ENV_TOKEN      : env-specific Kubernetes API token
# @arg ENV_CA_CERT    : env-specific Kubernetes CA certificate
# @arg ENV_KUBE_CONFIG: env-specific Kubeconfig
.k8s-cleanup:
  extends: .k8s-base
  stage: deploy
  # force no dependencies
  dependencies: []
  variables:
    ENV_APP_SUFFIX: "-$CI_ENVIRONMENT_SLUG"
  before_script:
    - *k8s-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - assert_defined "${ENV_SPACE:-$K8S_SPACE}" 'Missing required env $ENV_SPACE or $K8S_SPACE'
    - login "$ENV_TYPE" "${ENV_API_URL:-$K8S_URL}" "${ENV_CA_CERT:-$K8S_CA_CERT}" "${ENV_TOKEN:-$K8S_TOKEN}" "${ENV_SPACE:-$K8S_SPACE}" "${ENV_KUBE_CONFIG:-${K8S_DEFAULT_KUBE_CONFIG}}"
  script:
    - cleanup "$ENV_TYPE" ${ENV_APP_NAME:-${K8S_BASE_APP_NAME}${ENV_APP_SUFFIX}}
  environment:
    action: stop

k8s-review:
  extends: .k8s-deploy
  variables:
    ENV_TYPE: review
    ENV_APP_NAME: "$K8S_REVIEW_APP_NAME"
    ENV_API_URL: "$K8S_REVIEW_URL"
    ENV_SPACE: "$K8S_REVIEW_SPACE"
    ENV_TOKEN: "$K8S_REVIEW_TOKEN"
    ENV_CA_CERT: "$K8S_REVIEW_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_REVIEW_KUBE_CONFIG"
    ENV_URL: "${K8S_REVIEW_ENVIRONMENT_URL}"
    ENV_URL_LEGACY: "${K8S_REVIEW_ENVIRONMENT_SCHEME}://${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${K8S_REVIEW_ENVIRONMENT_DOMAIN}"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    on_stop: k8s-cleanup-review
  resource_group: review/$CI_COMMIT_REF_NAME
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # only on non-production, non-integration branches, with $K8S_REVIEW_SPACE set
    - if: '$K8S_REVIEW_SPACE && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'

# stop review env (automatically triggered once branches are deleted)
k8s-cleanup-review:
  extends: .k8s-cleanup
  variables:
    ENV_TYPE: review
    ENV_APP_NAME: "$K8S_REVIEW_APP_NAME"
    ENV_API_URL: "$K8S_REVIEW_URL"
    ENV_SPACE: "$K8S_REVIEW_SPACE"
    ENV_TOKEN: "$K8S_REVIEW_TOKEN"
    ENV_CA_CERT: "$K8S_REVIEW_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_REVIEW_KUBE_CONFIG"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  resource_group: review/$CI_COMMIT_REF_NAME
  rules:
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # only on non-production, non-integration branches, with $K8S_REVIEW_SPACE set
    - if: '$K8S_REVIEW_SPACE && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: manual
      allow_failure: true

k8s-integration:
  extends: .k8s-deploy
  variables:
    ENV_TYPE: integration
    ENV_APP_NAME: "$K8S_INTEG_APP_NAME"
    ENV_API_URL: "$K8S_INTEG_URL"
    ENV_SPACE: "$K8S_INTEG_SPACE"
    ENV_TOKEN: "$K8S_INTEG_TOKEN"
    ENV_CA_CERT: "$K8S_INTEG_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_INTEG_KUBE_CONFIG"
    ENV_URL: "${K8S_INTEG_ENVIRONMENT_URL}"
  environment:
    name: integration
  resource_group: integration
  rules:
    # only on integration branch(es), with $K8S_INTEG_SPACE set
    - if: '$K8S_INTEG_SPACE && $CI_COMMIT_REF_NAME =~ $INTEG_REF'

###############################
# Staging deploys are disabled by default since
# continuous deployment to production is enabled by default
# If you prefer to automatically deploy to staging and
# only manually promote to production, enable this job by setting
# $K8S_STAGING_SPACE.

k8s-staging:
  extends: .k8s-deploy
  variables:
    ENV_TYPE: staging
    ENV_APP_NAME: "$K8S_STAGING_APP_NAME"
    ENV_API_URL: "$K8S_STAGING_URL"
    ENV_SPACE: "$K8S_STAGING_SPACE"
    ENV_TOKEN: "$K8S_STAGING_TOKEN"
    ENV_CA_CERT: "$K8S_STAGING_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_STAGING_KUBE_CONFIG"
    ENV_URL: "${K8S_STAGING_ENVIRONMENT_URL}"
  environment:
    name: staging
  resource_group: staging
  rules:
    # only on production branch(es), with $K8S_STAGING_SPACE set
    - if: '$K8S_STAGING_SPACE && $CI_COMMIT_REF_NAME =~ $PROD_REF'

# deploy to production if on branch master and variable K8S_PROD_SPACE defined and AUTODEPLOY_TO_PROD is set
k8s-production:
  extends: .k8s-deploy
  stage: production
  variables:
    ENV_TYPE: production
    ENV_APP_SUFFIX: "" # no suffix for prod
    ENV_APP_NAME: "$K8S_PROD_APP_NAME"
    ENV_API_URL: "$K8S_PROD_URL"
    ENV_SPACE: "$K8S_PROD_SPACE"
    ENV_TOKEN: "$K8S_PROD_TOKEN"
    ENV_CA_CERT: "$K8S_PROD_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_PROD_KUBE_CONFIG"
    ENV_URL: "${K8S_PROD_ENVIRONMENT_URL}"
  environment:
    name: production
  resource_group: production
  rules:
    # exclude non-production branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF'
      when: never
    # exclude if $K8S_PROD_SPACE not set
    - if: '$K8S_PROD_SPACE == null || $K8S_PROD_SPACE == ""'
      when: never
    # if $AUTODEPLOY_TO_PROD: auto
    - if: '$AUTODEPLOY_TO_PROD == "true"'
    # else if PUBLISH_ON_PROD enabled: auto (because the publish job was blocking)
    - if: '$PUBLISH_ON_PROD == "true"'
    # else: manual, blocking
    - if: $K8S_PROD_SPACE # useless test, just to prevent GitLab warning
      when: manual
