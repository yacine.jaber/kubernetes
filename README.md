# GitLab CI template for Kubernetes (k8s)

This project implements a GitLab CI/CD template to deploy your application to a [Kubernetes](https://kubernetes.io/) platform 
using [declarative configuration](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/) 
or [Kustomize](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/kubernetes'
    ref: '3.4.3'
    file: '/templates/gitlab-ci-k8s.yml'
```

## Understand

This chapter introduces key notions and principle to understand how this template works.

### Managed deployment environments

This template implements continuous delivery/continuous deployment for projects hosted on Kubernetes platforms.

It allows you to manage automatic deployment & cleanup of standard predefined environments.
Each environment can be enabled/disabled by configuration.
If you're not satisfied with predefined environments and/or their associated Git workflow, you may implement you own environments and
workflow, by reusing/extending the base (hidden) jobs. This is advanced usage and will not be covered by this documentation.

The following chapters present the managed predefined environments and their associated Git workflow.

#### Review environments

The template supports **review** environments: those are dynamic and ephemeral environments to deploy your
_ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

When enabled, it deploys the result from upstream build stages to a dedicated and temporary environment.
It is only active for non-production, non-integration branches.

It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature.

It also comes with a _cleanup_ job (accessible either from the _environments_ page, or from the pipeline view).

#### Integration environment

If you're using a Git Workflow with an integration branch (such as [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)),
the template supports an **integration** environment.

When enabled, it deploys the result from upstream build stages to a dedicated environment.
It is only active for your integration branch (`develop` by default).

#### Production environments

Lastly, the template supports 2 environments associated to your production branch (`master` or `main` by default):

* a **staging** environment (an iso-prod environment meant for testing and validation purpose),
* the **production** environment.

You're free to enable whichever or both, and you can also choose your deployment-to-production policy:

* **continuous deployment**: automatic deployment to production (when the upstream pipeline is successful),
* **continuous delivery**: deployment to production can be triggered manually (when the upstream pipeline is successful).

### Supported authentication methods

The Kubernetes template supports 3 ways of login/accessing your Kubernetes cluster(s):

1. The [GitLab Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/): when enabled, the template
  automatically retrieves and uses your Kubernetes cluster configuration (`KUBECONFIG` env),
2. By defining an explicit `kubeconfig` from env (either file or yaml content),
3. By defining explicit `kubeconfig` **exploded parameters** from env: server url, server certificate authority and user token.

### Deployment context variables

In order to manage the various deployment environments, this template provides a couple of **dynamic variables**
that you might use in your hook scripts, deployment manifests and other deployment resources:

* `${environment_type}`: the current deployment environment type (`review`, `integration`, `staging` or `production`)
* `${environment_name}`: a generated application name to use for the current deployment environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`) - _details below_
* `${environment_name_ssc}`: the above application name in [SCREAMING_SNAKE_CASE](https://en.wikipedia.org/wiki/Snake_case) format
    (ex: `MYPROJECT_REVIEW_FIX_BUG_12` or `MYPROJECT_STAGING`)

#### Generated environment name

The `${environment_name}` variable is generated to designate each deployment environment with a unique and meaningful application name.
By construction, it is suitable for inclusion in DNS, URLs, Kubernetes labels...
It is built from:

* the application _base name_ (defaults to `$CI_PROJECT_NAME` but can be overridden globally and/or per deployment environment - _see configuration variables_)
* GitLab predefined `$CI_ENVIRONMENT_SLUG` variable ([sluggified](https://en.wikipedia.org/wiki/Clean_URL#Slug) name, truncated to 24 characters)

The `${environment_name}` variable is then evaluated as:

* `<app base name>` for the production environment
* `<app base name>-$CI_ENVIRONMENT_SLUG` for all other deployment environments
* :bulb: `${environment_name}` can also be overriden per environment with the appropriate configuration variable

Examples (with an application's base name `myapp`):

| `$environment_type` | Branch        | `$CI_ENVIRONMENT_SLUG`  | `$environment_name` |
|---------------------|---------------|-------------------------|---------------------|
| `review`            | `feat/blabla` | `review-feat-bla-xmuzs6`| `myapp-review-feat-bla-xmuzs6` |
| `integration`       | `develop`     | `integration`           | `myapp-integration` |
| `staging`           | `main`        | `staging`               | `myapp-staging` |
| `production`        | `main`        | `production`            | `myapp` |

### Supported deployment methods

The Kubernetes template supports three techniques to deploy your code:

1. script-based deployment,
2. template-based deployment using raw Kubernetes manifests (with variables substitution),
3. template-based deployment using [Kustomization files](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/).

#### 1: script-based deployment

In this mode, you only have to provide a shell script that fully implements the deployment using the [`kubectl` CLI](https://kubernetes.io/docs/reference/kubectl/overview/).

The deployment script is searched as follows:

1. look for a specific `k8s-deploy-$environment_type.sh` in the `$K8S_SCRIPTS_DIR` directory in your project (e.g. `k8s-deploy-staging.sh` for staging environment),
2. if not found: look for a default `k8s-deploy.sh` in the `$K8S_SCRIPTS_DIR` directory in your project,
3. if not found: the GitLab CI template assumes you're using the template-based deployment policy.

:warning: `k8s-deploy-$environment_type.sh` or `k8s-deploy.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-deploy.sh`

#### 2: template-based deployment

In this mode, you have to provide a [Kubernetes deployment file](https://kubernetes.io/docs/concepts/workloads/controllers/deployment)
in your project structure, and let the GitLab CI template [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it.

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-apply.sh` script in your project to perform specific environment pre-initialization (for e.g. create required services),
2. looks for your Kubernetes deployment file, performs [variables substitution](#using-variables) and [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. look for a specific `deployment-$environment_type.yml` in your project (e.g. `deployment-staging.yml` for staging environment),
    2. fallbacks to default `deployment.yml`.
3. _optionally_ executes the `k8s-post-apply.sh` script in your project to perform specific environment post-initialization stuff,

:warning: `k8s-pre-apply.sh` or `k8s-post-apply.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-apply.sh`

#### 3: Kustomize-based deployment

In this mode, you have to provide a [Kustomization file](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/)
in your project structure, and let the template [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it.

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-apply.sh` script in your project to perform specific environment pre-initialization (for e.g. create required services),
2. looks for your Kustomization file, performs variables substitution and [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. looks for an environment-specific [overlay](https://kubectl.docs.kubernetes.io/references/kustomize/glossary/#overlay) file `./$environment_type/kustomization.yaml` (e.g. `./staging/kustomization.yaml ` for staging environment),
    2. fallbacks to default `kustomization.yaml`.
3. _optionally_ executes the `k8s-post-apply.sh` script in your project to perform specific environment post-initialization stuff,

:warning: `k8s-pre-apply.sh` or `k8s-post-apply.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-apply.sh`

Variables substitution is performed by the deprecated feature from Kustomize based on `configMapGenerator`, using a non-valuated variable from a config map.

#### Readiness script

After deployment (either script-based or template-based), the GitLab CI template _optionally_ executes the `k8s-readiness-check.sh` hook script to wait & check for the application to be ready (if not found, the template assumes the application was successfully started).

:warning: `k8s-readiness-check.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-apply.sh`

### Supported cleanup methods

The Kubernetes template supports three techniques to destroy an environment (actually only review environments):

1. script-based deployment,
2. template-based deployment using raw Kubernetes manifests (with variables substitution),
3. template-based deployment using [Kustomization files](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/).

#### 1: script-based cleanup

In this mode, you only have to provide a shell script that fully implements the environment cleanup using the [`kubectl` CLI](https://kubernetes.io/docs/reference/kubectl/overview/).

The a deployment script is searched as follows:

1. look for a specific `k8s-cleanup-$environment_type.sh` in the `$K8S_SCRIPTS_DIR` directory in your project (e.g. `k8s-cleanup-staging.sh` for staging environment),
2. if not found: look for a default `k8s-cleanup.sh` in the `$K8S_SCRIPTS_DIR` directory in your project,
3. if not found: the GitLab CI template assumes you're using the template-based cleanup policy.

:warning: `k8s-cleanup-$environment_type.sh` or `k8s-cleanup.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-cleanup.sh`

> TIP: a nice way to implement environment cleanup is to declare the label `app=${environment_name}` on every Kubernetes
> object associated to your environment.
> Then environment cleanup can be implemented very easily with command `kubectl delete all,pvc,secret,ingress -l "app=${environment_name}"`

#### 2: template-based cleanup

In this mode, you mainly let Kubernetes delete all objects from your Kubernetes deployment file.

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-cleanup.sh` script in your project to perform specific environment pre-cleanup stuff,
2. looks for your Kubernetes deployment file, performs [variables substitution](#using-variables) and [`kubectl delete`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. look for a specific `deployment-$environment_type.yml` in your project (e.g. `deployment-staging.yml` for staging environment),
    2. fallbacks to default `deployment.yml`.
3. _optionally_ executes the `k8s-post-cleanup.sh` script in your project to perform specific environment post-cleanup (for e.g. delete bound services).

:warning: `k8s-pre-cleanup.sh` or `k8s-post-cleanup.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-cleanup.sh`

#### 3: Kustomize-based cleanup

In this mode, you mainly let Kubernetes delete all objects from your [Kustomization file(s)](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/).

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-cleanup.sh` script in your project to perform specific environment pre-cleanup stuff,
2. looks for your Kustomization file, performs variables substitution and [`kubectl delete`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. looks for an environment-specific [overlay](https://kubectl.docs.kubernetes.io/references/kustomize/glossary/#overlay) file `./$environment_type/kustomization.yaml` (e.g. `./staging/kustomization.yaml ` for staging environment),
    2. fallbacks to default `kustomization.yaml`.
3. _optionally_ executes the `k8s-post-cleanup.sh` script in your project to perform specific environment post-cleanup (for e.g. delete bound services).

:warning: `k8s-pre-cleanup.sh` or `k8s-post-cleanup.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-cleanup.sh`

#### Cleanup job limitations

When using this template, you have to be aware of one limitation (bug) with the cleanup job.

By default, the cleanup job triggered automatically on branch deletion will **fail** due to not being able
to fetch the Git branch prior to executing the job (obviously because the branch was just deleted).
This is pretty annoying, but as you may see above, deleting an env _may_ require scripts from the project...

In a future version, the template will rely on [Kustomize](https://kustomize.io/) and will be able to delete an
entire environment using the `app` label. In the meantime we're just sorry about this bug, but for now there is not
much we can do:

* remind to delete your review env **manually before deleting the branch**
* otherwise you'll have to do it afterwards from your computer (using `kubectl` CLI) or from the some k8s web console.

### Using variables

You have to be aware that your deployment (and cleanup) scripts have to be able to cope with various environments, each 
with different application names, exposed routes, settings, ...
Part of this complexity can be handled by the lookup policies described above (ex: one script/manifest per env) and also 
by using available environment variables:

1. [deployment context variables](#deployment-context-variables) provided by the template:
    * `${environment_type}`: the current environment type (`review`, `integration`, `staging` or `production`)
    * `${environment_name}`: the application name to use for the current environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`)
    * `${environment_name_ssc}`: the application name in [SCREAMING_SNAKE_CASE](https://en.wikipedia.org/wiki/Snake_case) format
       (ex: `MYPROJECT_REVIEW_FIX_BUG_12` or `MYPROJECT_STAGING`)
    * `${hostname}`: the environment hostname, extracted from the current environment url (after late variable expansion - see below)
2. any [GitLab CI variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
3. any [custom variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
    (ex: `${SECRET_TOKEN}` that you have set in your project CI/CD variables)

While your scripts may simply use any of those variables, your Kubernetes and Kustomize resources can use **variable substitution**
with the syntax `${VARIABLE_NAME}`.
Each of those patterns will be dynamically replaced in your resources by the template right before using it.

You can prevent any line from being processed by appending `# nosubst` at the end of the line.
For instance in the following example, `${REMOTE_SERVICE_NAME}` won't be replaced by its environment value during GitLab job execution:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  labels:
    app: ${APPLICATION_NAME}
  name: ${APPLICATION_NAME}
data:
  application.yml: |
    remote:
      some-service:
          name: '${REMOTE_SERVICE_NAME}' # nosubst
```

> :warning: In order to be properly replaced, curly braces are mandatory (ex: `${MYVAR}` and not `$MYVAR`).
> Moreover, multiline variables must be surrounded by **double quotes** (`"`).
>
> Example:
>
> ```yaml
> [...]
> containers:
> - name: restaurant-app
>   env:
>   # multiline variable
>   - name: MENU
>     value: "${APP_MENU}"
> ```

### Environments URL management

The K8S template supports two ways of providing your environments url:

* a **static way**: when the environments url can be determined in advance, probably because you're exposing your routes through a DNS you manage,
* a [**dynamic way**](https://docs.gitlab.com/ee/ci/environments/#set-dynamic-environment-urls-after-a-job-finishes): when the url cannot be known before the
  deployment job is executed.

The **static way** can be implemented simply by setting the appropriate configuration variable(s) depending on the environment (see environments configuration chapters):

* `$K8S_ENVIRONMENT_URL` to define a default url pattern for all your envs,
* `$K8S_REVIEW_ENVIRONMENT_URL`, `$K8S_INTEG_ENVIRONMENT_URL`, `$K8S_STAGING_ENVIRONMENT_URL` and `$K8S_PROD_ENVIRONMENT_URL` to override the default.

> :information_source: Each of those variables support a **late variable expansion mechanism** with the `%{somevar}` syntax, 
> allowing you to use any dynamically evaluated variables such as `${environment_name}`.
>
> Example:
>
> ```yaml
> variables:
>   K8S_BASE_APP_NAME: "wonderapp"
>   # global url for all environments
>   K8S_ENVIRONMENT_URL: "https://%{environment_name}.nonprod.acme.domain"
>   # override for prod (late expansion of $K8S_BASE_APP_NAME not needed here)
>   K8S_PROD_ENVIRONMENT_URL: "https://$K8S_BASE_APP_NAME.acme.domain"
>   # override for review (using separate resource paths)
>   K8S_REVIEW_ENVIRONMENT_URL: "https://wonderapp-review.nonprod.acme.domain/%{environment_name}"
> ```

To implement the **dynamic way**, your deployment script shall simply generate a `environment_url.txt` file in the working directory, containing only
the dynamically generated url. When detected by the template, it will use it as the newly deployed environment url.

### Deployment output variables

Each deployment job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

* `$environment_type`: set to the type of environment (`review`, `integration`, `staging` or `production`),
* `$environment_name`: the application name (see below),
* `$environment_url`: set to the environment URL (whether determined statically or dynamically).

Those variables may be freely used in downstream jobs (for instance to run acceptance tests against the latest deployed environment).

## Configuration reference

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).

### Global configuration

The Kubernetes template uses some global configuration used throughout all jobs.

| Name                  | description                            | default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `K8S_KUBECTL_IMAGE`    | the Docker image used to run Kubernetes `kubectl` commands <br/>:warning: **set the version required by your Kubernetes server** | `registry.hub.docker.com/bitnami/kubectl:latest` |
| `K8S_BASE_APP_NAME`    | default application name              | `$CI_PROJECT_NAME` ([see GitLab doc](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)) |
| `K8S_ENVIRONMENT_URL`    | Default environments url _(only define for static environment URLs declaration)_<br/>_supports late variable expansion (ex: `https://%{environment_name}.k8s.acme.com`)_ | _none_ |
| :lock: `K8S_DEFAULT_KUBE_CONFIG`| The default kubeconfig to use (either content or file variable) | **required if not using exploded kubeconfig parameters** |
| `K8S_URL`              | the Kubernetes API url                | **required if using exploded kubeconfig parameters** |
| `K8S_CA_CERT`          | the default Kubernetes server certificate authority | **optional if using exploded kubeconfig parameters** |
| :lock: `K8S_TOKEN`     | default service account token         | **required if using exploded kubeconfig parameters** |
| `K8S_SCRIPTS_DIR`      | directory where k8s scripts (hook scripts) are located | `.` _(root project dir)_ |
| `K8S_KUSTOMIZE_ENABLED` | Set to `true` to force using [Kustomize](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/) | _none_ (disabled) |
| `DOCKER_CONTAINER_STABLE_IMAGE` | Docker image name to use for staging/prod | **has to be defined when not chaining execution from Docker template** |
| `DOCKER_CONTAINER_UNSTABLE_IMAGE` | Docker image name to use for review | **has to be defined when not chaining execution from Docker template** |

### Review environments configuration

Review environments are dynamic and ephemeral environments to deploy your _ongoing developments_ (a.k.a. _feature_ or
_topic_ branches).

They are **disabled by default** and can be enabled by setting the `K8S_REVIEW_SPACE` variable (see below).

Here are variables supported to configure review environments:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `K8S_REVIEW_SPACE`       | k8s namespace for `review` env         | _none_ (disabled) |
| `K8S_REVIEW_APP_NAME`    | application name for `review` env      | `"${K8S_BASE_APP_NAME}-${CI_COMMIT_REF_SLUG}"` |
| `K8S_REVIEW_ENVIRONMENT_URL`| The review environments url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_REVIEW_KUBE_CONFIG` | Specific kubeconfig for `review` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `K8S_REVIEW_URL`         | Kubernetes API url for `review` env  _(only define if using exploded kubeconfig parameters and if different from default)_    | `$K8S_URL` |
| `K8S_REVIEW_CA_CERT`     | the Kubernetes server certificate authority for `review` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_REVIEW_TOKEN`| service account token for `review` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |

### Integration environment configuration

The integration environment is the environment associated to your integration branch (`develop` by default).

It is **disabled by default** and can be enabled by setting the `K8S_INTEG_SPACE` variable (see below).

Here are variables supported to configure the integration environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `K8S_INTEG_SPACE`        | k8s namespace for `integration` env    | _none_ (disabled) |
| `K8S_INTEG_APP_NAME`     | application name for `integration` env | `$K8S_BASE_APP_NAME-integration` |
| `K8S_INTEG_ENVIRONMENT_URL`| The integration environment url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_INTEG_KUBE_CONFIG` | Specific kubeconfig for `integration` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `K8S_INTEG_URL`          | Kubernetes API url for `integration` env  _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_URL` |
| `K8S_INTEG_CA_CERT`      | the Kubernetes server certificate authority for `integration` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_INTEG_TOKEN` | service account token for `integration` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |

### Staging environment configuration

The staging environment is an iso-prod environment meant for testing and validation purpose associated to your production branch (`master` by default).

It is **disabled by default** and can be enabled by setting the `K8S_STAGING_SPACE` variable (see below).

Here are variables supported to configure the staging environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `K8S_STAGING_SPACE`      | k8s namespace for `staging` env        | _none_ (disabled) |
| `K8S_STAGING_APP_NAME`   | application name for `staging` env     | `$K8S_BASE_APP_NAME-staging` |
| `K8S_STAGING_ENVIRONMENT_URL`| The staging environment url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_STAGING_KUBE_CONFIG` | Specific kubeconfig for `staging` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `K8S_STAGING_URL`        | Kubernetes API url for `staging` env  _(only define if using exploded kubeconfig parameters and if different from default)_   | `$K8S_URL` |
| `K8S_STAGING_CA_CERT`    | the Kubernetes server certificate authority for `staging` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_STAGING_TOKEN`| service account token for `staging` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |

### Production environment configuration

The production environment is the final deployment environment associated with your production branch (`master` by default).

It is **disabled by default** and can be enabled by setting the `K8S_PROD_SPACE` variable (see below).

Here are variables supported to configure the production environment:

| Name                     | description                            | default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `K8S_PROD_SPACE`         | k8s namespace for `production` env     | _none_ (disabled) |
| `K8S_PROD_APP_NAME`      | application name for `production` env  | `$K8S_BASE_APP_NAME` |
| `K8S_PROD_ENVIRONMENT_URL`| The production environment url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_PROD_KUBE_CONFIG` | Specific kubeconfig for `production` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `K8S_PROD_URL`           | Kubernetes API url for `production` env  _(only define if using exploded kubeconfig parameters and if different from default)_| `$K8S_URL` |
| `K8S_PROD_CA_CERT`       | the Kubernetes server certificate authority for `production` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_PROD_TOKEN`  | service account token for `production` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |
| `AUTODEPLOY_TO_PROD`     | Set this variable to auto-deploy to production. If not set deployment to production will be `manual` (default behaviour). | _none_ (disabled) |

### kube-score job

The Kubernetes template enables [kube-score](https://github.com/zegl/kube-score) analysis of your Kubernetes object definitions.
This job is mapped to the `test` stage and is **active** by default.

Here are its parameters:

| Name                   | description                                                          | default value     |
| ---------------------- | -------------------------------------------------------------------- | ----------------- |
| `K8S_KUBE_SCORE_IMAGE` | Docker image to run [kube-score](https://github.com/zegl/kube-score) | `registry.hub.docker.com/zegl/kube-score:latest-kustomize` **it is recommended to set a tool version compatible with your Kubernetes cluster** |
| `K8S_SCORE_DISABLED`   | Set to `true` to disable the `kube-score` analysis                             | _none_ (enabled) |
| `K8S_SCORE_EXTRA_OPTS` | [Additional options](https://github.com/zegl/kube-score#configuration) to `kube-score` command line | _none_ |

## Variants

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Name              | description                            | default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `$CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master` |
| `VAULT_BASE_URL`  | The Vault server base API url          | _none_ |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Name                             | description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - project: 'to-be-continuous/kubernetes'
    ref: '3.4.3'
    file: '/templates/gitlab-ci-k8s.yml'
  # Vault variant
  - project: 'to-be-continuous/kubernetes'
    ref: '3.4.3'
    file: '/templates/gitlab-ci-k8s-vault.yml'

variables:
    # Secrets managed by Vault
    K8S_DEFAULT_KUBE_CONFIG: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-app/kubernetes/noprod?field=kube_config"
    K8S_PROD_KUBE_CONFIG: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-app/kubernetes/prod?field=kube_config"
    VAULT_BASE_URL: "https://vault.acme.host/v1"
    # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable
```
